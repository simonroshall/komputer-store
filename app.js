const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loanBalance");
const getLoanElement = document.getElementById("loanButton");
const payElement = document.getElementById("pay");
const bankElement = document.getElementById("bankButton");
const workElement = document.getElementById("workButton");
const selectedLaptopElement = document.getElementById("selectedLaptop");
const featureElement = document.getElementById("features");
const imageElement = document.getElementById("image");
const computerNameElement = document.getElementById("computerName");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyButtonElement = document.getElementById("buyButton");
const payLoanButtonElement = document.getElementById("payLoanButton");


let workBalance = 0;
let bankBalance = 0;
let loan = 0;
let computers = [];

//Fetching data from the API 
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers))

//Loops the computers from the fetch and adds them into the scroll down list.
const addComputers = (computers) => {
    computers.forEach(x => addComputersToList(x));
    featureElement.innerText = computers[0].specs;
    imageElement.innerText = computers[0].image;
    computerNameElement.innerText = computers[0].title;
    descriptionElement.innerText = computers[0].description;
    priceElement.innerText = computers[0].price + " SEK";
    
    const img = document.getElementById("image");
    img.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
}

const addComputersToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    selectedLaptopElement.appendChild(computerElement);
}

//Updates the computer information when changing in the dropdown. 
const handleComputerChange = e => {
    const selectedLaptop = computers[e.target.selectedIndex];
    featureElement.innerText = selectedLaptop.specs;

    //As image with id 5 have the wrong format, I had to change jpg to png with the replace() function.
    if(selectedLaptop.id == 5){
        var url = selectedLaptop.image.replace('jpg','png');
        const img = document.getElementById("image");
        img.src = "https://noroff-komputer-store-api.herokuapp.com/" + url;
    }
    else{
    const img = document.getElementById("image");
    img.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;

    computerNameElement.innerText = selectedLaptop.title;
    descriptionElement.innerText = selectedLaptop.description;
    priceElement.innerText = selectedLaptop.price + " SEK";
}
}
//Adds 100 kr to pay balance when pressing work and updates the element. 
const handleWork = () => {
    workBalance += 100;
    payElement.innerText = `Pay: ${workBalance} kr`;
}

//Function to transfer money from pay to bank. If you have a loan pending, 10% of the transfer goes to paying off the loan. 
const bankYourPay = () => {
    if(loan > 0){
        paymentOnLoan = workBalance*0.1;
        loan -= paymentOnLoan;
        bankBalance += workBalance - paymentOnLoan;
        balanceElement.innerText = `Balance: ${bankBalance} kr`;
        loanElement.innerText = `Loan debt: ${loan} kr`;
        workBalance = 0; 
        payElement.innerText = `Pay: ${workBalance} kr`;
        
        if (loan == 0){
            document.getElementById("payLoanButton").hidden = true;
            loanElement.innerText = ``;
    }
    }
    else{
        bankBalance += workBalance;
        balanceElement.innerText = `Balance: ${bankBalance} kr`;
        workBalance = 0;
        payElement.innerText = `Pay: ${workBalance} kr`;
}}

//Function to handle loan requests. The user can only have one loan at once and the loan can max be two times bigger than their bank blance. 
const handleLoan = () => {
    const sizeOfLoan = prompt("Enter the amount of money you wish to loan: ")
    if(sizeOfLoan > 0){
    
    if(loan > 0){
        alert("You have to pay your loan before taking another!")
    }
    else{ 
    if (bankBalance >= (sizeOfLoan / 2)) {
        loan = sizeOfLoan;
        loanElement.innerText = `Loan debt: ${loan} kr`;
        document.getElementById("payLoanButton").hidden = false;
        bankBalance += parseInt(sizeOfLoan);
        balanceElement.innerText = `Balance: ${bankBalance} kr`;
        alert("You have lended " + sizeOfLoan);
        
    } else { 
        alert("You can't take a loan that's double the amount than you bank balance");
    }
        }
}
}

//Function for paying off the loan. Checking if the loan is bigger or smaller than the work balance (pay)
const payLoan = () => {
    if(loan <= workBalance){
        workBalance -= loan; 
        loan = 0;
        loanElement.innerText = ``;
        payElement.innerText = `Pay: ${workBalance} kr`;
        document.getElementById("payLoanButton").hidden = true;

    }
    else{
    loan -= workBalance;
    workBalance = 0;
    loanElement.innerText = `Loan debt: ${loan} kr`;
    payElement.innerText = `Pay: ${workBalance} kr`;
        }
                        }           

//Function for buying an computer.
const buyComputer = () => {
    const computerPrice = document.getElementById("price").textContent.split(" ")[0];
    const computerName = document.getElementById("computerName").textContent;

    if(bankBalance >= computerPrice){
        bankBalance -= computerPrice;
        balanceElement.innerText = `Balance ${bankBalance} kr`;
        alert("You now own the computer: " + computerName);
    }
    else{
        alert("You cannot afford the computer!");
    }
}

//Adds Eventlisteners
workElement.addEventListener("click", handleWork);
getLoanElement.addEventListener("click", handleLoan);
bankElement.addEventListener("click", bankYourPay);
selectedLaptopElement.addEventListener("change", handleComputerChange);
payLoanButtonElement.addEventListener("click", payLoan);
buyButtonElement.addEventListener("click", buyComputer);